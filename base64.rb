#!/usr/bin/ruby

require 'base64'

method_name = ARGV[0].to_s + '64'

if Base64.respond_to? method_name
  print Base64.send(method_name.to_sym, STDIN.read)
else
  puts '[strict_|urlsafe_|]encode|decode'
end